// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MapManager.h"
#include "MinimapWidget.h"

#include "ZLProjGameMode.generated.h"

/**
 * Basic map lifecycle management for demonstration of the MapTools plugin.
 */
UCLASS(minimalapi)
class AZLProjGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AZLProjGameMode();

	virtual void BeginPlay();
	virtual void EndPlay(const EEndPlayReason::Type Reason);

	UPROPERTY(BlueprintReadOnly, Transient, Category="Map")
	UMapManager* Map;

	UPROPERTY(EditDefaultsOnly, Category="Map")
	TSubclassOf<UMinimapWidget> MinimapClass;

	UPROPERTY(BlueprintReadOnly, Transient, Category="Map")
	UMinimapWidget* MinimapInstance;
};