// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "ZLProjGameMode.h"
#include "ZLProjCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "UserWidget.h"

AZLProjGameMode::AZLProjGameMode()
	:Map(nullptr)
	, MinimapInstance(nullptr)
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}

void AZLProjGameMode::BeginPlay()
{
	Super::BeginPlay();
	Map = NewObject<UMapManager>(this);
	Map->BeginPlay(GetWorld());
	if (MinimapClass)
	{
		MinimapInstance = NewObject<UMinimapWidget>(this, MinimapClass);
		MinimapInstance->SetMapManager(Map);
		MinimapInstance->AddToViewport();
	}
}

void AZLProjGameMode::EndPlay(const EEndPlayReason::Type Reason)
{
	// Let UCLASSes get garbage collected
	if (MinimapInstance)
	{
		MinimapInstance->RemoveFromViewport();
		MinimapInstance = nullptr;
	}

	Map = nullptr;
	Super::EndPlay(Reason);
}