// Fill out your copyright notice in the Description page of Project Settings.

#include "MapManager.h"
#include "UObjectIterator.h"
#include "GameFramework/Actor.h"
#include "MappableItem.h"

UMapManager::UMapManager()
{
}

UMapManager::~UMapManager()
{
}

void UMapManager::BeginPlay(UWorld* inWorld)
{
	TrackedItems.Reset();
	for (TObjectIterator<UMappableItem> itr; itr; ++itr)
	{
		if (itr->GetWorld() == inWorld && itr->IsActive())
		{
			// Has already activated, we need to start tracking it
			Track(*itr);
		}
	}

}

void UMapManager::Track(UMappableItem* item)
{
	TrackedItems.Add(item);
}

void UMapManager::Untrack(UMappableItem* item)
{
	TrackedItems.Remove(item);
}

void UMapManager::SetMapImage(UTexture2D* image, const FBox2D& mapWorldBounds)
{
	MapTexture = image;
	MapTextureBounds = mapWorldBounds;
}

UTexture2D* UMapManager::GetMapImage(FBox2D& outMapWorldBounds) const
{
	outMapWorldBounds = MapTextureBounds;
	return MapTexture;
}

TArray<FMapItem> UMapManager::GetObjectsWithin(float dist, const FVector& point)
{
	const float DistSqThreshold = dist * dist;
	TArray<FMapItem> list;
	FMapItem newItem;

	for (UMappableItem* itr : TrackedItems)
	{
		if (!itr->IsActive())
			continue;

		newItem.Position = itr->GetOwner()->GetTransform().GetLocation() - point;
		
		// For a 2d map we're only interested in XY
		newItem.Position.Z = 0.f;
		float DistSq = newItem.Position.SizeSquared2D();

		if (DistSq < DistSqThreshold)
		{
			newItem.Distance = FMath::Sqrt(DistSq);
			newItem.Type = itr->MapType;
			newItem.Facing = itr->GetOwner()->GetActorRotation().Yaw;
			list.Push(newItem);
		}
	}

	return list;
}