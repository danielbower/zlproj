#include "MappableItem.h"
#include "UObjectIterator.h"
#include "MapManager.h"


UMappableItem::UMappableItem()
{
	PrimaryComponentTick.bCanEverTick = false;
	bAutoActivate = true;
}

void UMappableItem::Activate(bool bReset)
{
	bool wasActive = bIsActive;
	Super::Activate(bReset);
	if (!wasActive && bIsActive)
	{
		for (TObjectIterator<UMapManager> itr; itr; ++itr)
		{
			itr->Track(this);
		}
	}
}

void UMappableItem::Deactivate()
{
	bool wasActive = bIsActive;
	Super::Deactivate();
	if (wasActive && !bIsActive)
	{
		for (TObjectIterator<UMapManager> itr; itr; ++itr)
		{
			itr->Untrack(this);
		}
	}
}