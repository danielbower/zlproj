// Fill out your copyright notice in the Description page of Project Settings.

#include "MinimapWidget.h"
#include "MapManager.h"
#include "Components/Image.h"
#include "Components/CanvasPanel.h"
#include "Components/CanvasPanelSlot.h"
#include "Engine/Texture2D.h"


UMinimapWidget::UMinimapWidget(const FObjectInitializer& ObjectInitializer)
	: UUserWidget(ObjectInitializer)
	, MapScale(1.0f)
	, IconSize(16.f)
	, MapManager(nullptr)
	, IconPool(nullptr, 0)
{

}

void UMinimapWidget::SetMapManager(UMapManager* NewManager)
{
	MapManager = NewManager;
}

void UMinimapWidget::SetMaxItems(int max)
{
	int oldCount = IconPool.Num();

	// Clean up unneeded icons
	for (int i = oldCount - 1; i >= max; --i)
	{
		if (IconHolder)
			IconPool[i]->RemoveFromParent();

		IconPool[i] = nullptr;
	}

	IconPool.SetNum(max, true);

	// Add new icons
	for (int i = oldCount; i < max; ++i)
	{
		IconPool[i] = NewObject<UImage>(this);

		if (IconHolder)
		{
			IconHolder->AddChild(IconPool[i]);

			// Center it.
			UCanvasPanelSlot* slot = Cast<UCanvasPanelSlot>(IconPool[i]->Slot);
			slot->SetAnchors(FAnchors(0.5f));
			slot->SetSize(FVector2D(1.f, 1.f));
			slot->SetZOrder(2);
		}
	}
}

void UMinimapWidget::SetIconHolder(UCanvasPanel* holder)
{
	// Reparent widgets as needed
	for (int i = 0; i < IconPool.Num(); ++i)
	{
		if (IconHolder)
			IconPool[i]->RemoveFromParent();
		if (holder)
		{
			holder->AddChild(IconPool[i]);
			// Center it.
			UCanvasPanelSlot* slot = Cast<UCanvasPanelSlot>(IconPool[i]->Slot);
			slot->SetAnchors(FAnchors(0.5f));
			slot->SetSize(FVector2D(1.f,1.f));
			slot->SetZOrder(2);
		}
	}

	// Set up the map image
	if (MapImage)
		MapImage->RemoveFromParent();
	else
		MapImage = NewObject<UImage>();

	if (holder)
	{
		holder->AddChild(MapImage);
		// Center it, and set halfextents to 1 to match the bounding box.
		UCanvasPanelSlot* slot = Cast<UCanvasPanelSlot>(MapImage->Slot);
		slot->SetAnchors(FAnchors(0.5f));
		slot->SetSize(FVector2D(2.f, 2.f));
		slot->SetZOrder(1); // Renders first, below the icons
	}

	IconHolder = holder;
}

void UMinimapWidget::RemoveFromParent()
{
	// Always clean up
	SetMaxItems(0);
}

void UMinimapWidget::UpdateMapAndIcons(const FVector& worldCentre, float relativeHeading, const TArray<FSlateBrush>& BrushesByType, float ScreenspaceRange)
{
	// Update backing map texture transform
	if (MapImage)
	{
		UTexture2D* MapTexture = nullptr;
		FBox2D Bounds;
		if (MapManager)
		{
			MapTexture = MapManager->GetMapImage(Bounds);
		}
		if (MapTexture != MapImage->Brush.GetResourceObject())
		{
			MapImage->SetBrushFromTexture(MapTexture);
		}

		float mapAngle = 90.f - relativeHeading; // Map rotates in the opposite direction to player heading, plus 90 degree offset from Game (1,0,0) == UI (0,-1)

		FWidgetTransform transform;
		transform.Angle = mapAngle;
		transform.Translation.Set(worldCentre.X, worldCentre.Y);
		transform.Translation -= Bounds.GetCenter();
		transform.Translation = transform.Translation.GetRotated(mapAngle) * MapScale;
		transform.Scale = MapScale * Bounds.GetExtent();
		MapImage->SetRenderTransform(transform);
	}

	// Update icons
	if (IconHolder)
	{
		TArray<FMapItem> mapItems;
		if (MapManager)
		{
			mapItems = MapManager->GetObjectsWithin(ScreenspaceRange / MapScale, worldCentre);
		}

		// Sort by distance
		mapItems.Sort();

		for (int i = 0; i < IconPool.Num(); ++i)
		{
			if (i >= mapItems.Num() || mapItems[i].Type < 0 || mapItems[i].Type >= BrushesByType.Num())
			{
				// If we haven't found this many items, or it has no valid brush set, hide it.
				IconPool[i]->SetVisibility(ESlateVisibility::Hidden);
			}
			else
			{
				IconPool[i]->SetVisibility(ESlateVisibility::Visible);
				IconPool[i]->SetBrush(BrushesByType[mapItems[i].Type]);

				FWidgetTransform transform;
				transform.Angle = mapItems[i].Facing - relativeHeading;
				transform.Translation.Set(mapItems[i].Position.X, mapItems[i].Position.Y);
				transform.Translation = transform.Translation.GetRotated(-90.f-relativeHeading) * MapScale; // Game world treats (1,0,0) as forwards, UI displays (0,-1) as "up"
				transform.Scale.Set(IconSize, IconSize);

				IconPool[i]->SetRenderTransform(transform);
			}
		}
	}
}