// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MappableItem.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MAPTOOLS_API UMappableItem : public UActorComponent
{
	GENERATED_BODY()

public:	
	UMappableItem();

protected:
	virtual void Activate(bool bReset) override;
	virtual void Deactivate() override;

public:	
	UPROPERTY(EditAnywhere, Category = "Map")
	int MapType;
};
