// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MinimapWidget.generated.h"

class UMapManager;
class UImage;
class UCanvasPanel;

/**
 * Provides C++ methods for fast processing of UMapManager data
 */
UCLASS()
class MAPTOOLS_API UMinimapWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	UMinimapWidget(const FObjectInitializer& ObjectInitializer);
	virtual void RemoveFromParent() override;

	/**
	* Once off set up functions
	*/
	UFUNCTION(BlueprintCallable, Category="Map")
	void SetMapManager(UMapManager* NewMap);

	UFUNCTION(BlueprintCallable, Category = "Map")
	void SetMaxItems(int max);

	UFUNCTION(BlueprintCallable, Category = "Map")
	void SetIconHolder(UCanvasPanel* holder);

	/**
	* Update the map - this could be called every tick, or throttled if you want it more like a radar ping.
	* BrushesByType - an array of possible brushes, indexed by UMappableItem::MapType. If Type is invalid, the icon will be hidden.
	* ScreenspaceRange - Maximum distance to display minimap icons in screenspace.
	*/
	UFUNCTION(BlueprintCallable, Category = "Map")
	void UpdateMapAndIcons(const FVector& worldCentre, float relativeHeading, const TArray<FSlateBrush>& BrushesByType, float ScreenspaceRange);

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Map", meta=(ClampMin = 0.001f, ClampMax = 10000.f, UIMin = 0.1f, UIMax = 5.f))
	float MapScale;

	UPROPERTY(EditAnywhere, Category = "Map")
	float IconSize;

private:
	UPROPERTY()
	UMapManager* MapManager;

	UPROPERTY()
	UCanvasPanel* IconHolder;

	UPROPERTY()
	UImage* MapImage;

	UPROPERTY()
	TArray<UImage*> IconPool;

};
