#pragma once

#include "CoreMinimal.h"
#include "MapManager.generated.h"

class UMappableItem;
class UTexture2D;

USTRUCT(BlueprintType)
struct FMapItem
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadWrite)
	FVector Position;

	UPROPERTY(BlueprintReadWrite)
	float Distance;

	UPROPERTY(BlueprintReadWrite)
	float Facing;
	
	UPROPERTY(BlueprintReadWrite)
	int Type;

	// For sorting...
	bool operator<(const FMapItem& rhs) const { return Distance < rhs.Distance; }
};

/**
 * Global class for tracking mapping data in a game.
 * Kept seperate from the UI implementation as there could be several such displays in the future (minimap/fullscreen menu)
 * and there shouldn't be a need to tie the data's lifetime to that of the UI.
 */
UCLASS(BlueprintType)
class MAPTOOLS_API UMapManager : public UObject
{
	GENERATED_BODY()
public:
	UMapManager();
	~UMapManager();

	void BeginPlay(UWorld* inWorld);
	void Track(UMappableItem* item);
	void Untrack(UMappableItem* item);

	UFUNCTION(BlueprintCallable, Category = "Map")
	void SetMapImage(UTexture2D* image, const FBox2D& mapWorldBounds);

	UFUNCTION(BlueprintCallable, Category = "Map")
	UTexture2D* GetMapImage(FBox2D& outMapWorldBounds) const;

	UFUNCTION(BlueprintCallable, Category = "Map")
	TArray<FMapItem> GetObjectsWithin(float dist, const FVector& point = FVector::ZeroVector);

private:
	TArray<UMappableItem*> TrackedItems;

	UPROPERTY()
	UTexture2D* MapTexture;
	FBox2D MapTextureBounds;
};
